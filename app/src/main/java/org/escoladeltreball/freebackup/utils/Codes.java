package org.escoladeltreball.freebackup.utils;

/**
 * Created by Arthur on 4/5/16.
 */
public class Codes {

    // -------  CONSTANTS FOR SHARED PREFERENCES  --------
    public static final String SHARED_PREFERENCES_NAME = "org.escoladeltreball.freebackup.SHARED_PREFERENCES";


    // -------  CONSTANTS FOR IMAGE'S CURSOR  --------
    public static final String NAME_OF_THUMBNAILS_FOLDER = ".thumbnails";

    //------------- Contacts ----------------

    public static final String VCF_FILE_CONTACTS_TO_IMPORT = "FreeBackupContacts.vcf";
    public static final String VCF_FILE_TO_EXPORT = "FreeBackupContacts.vcf";
    public static final String VCF_FILE_TO_IMPORT_TEMP = "FreeBackupContactsTemp.vcf";
}
