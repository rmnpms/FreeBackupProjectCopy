package org.escoladeltreball.freebackup.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

/**
 * Utils for general purpose.
 * <p/>
 * Created by Arthur on 25/4/16.
 */
public class Utils {

    /**
     * Show Toast.
     *
     * @param context the context.
     * @param message the message for show.
     */
    public static void showToast(Context context, String message) {
        if (context != null && !message.isEmpty()) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    //-------------------------------------- Gallery Methods -------------------------------

    /**
     * Get cursor without filter from an Uri.
     *
     * @param context    The Context.
     * @param uri        The URI.
     * @param projection A list of which columns to return.
     * @param sortOrder  To order the rows. Passing null to default sort order.
     * @return The cursor with result.
     */
    public static Cursor getCursorFromUri(Context context, @NonNull Uri uri, @Nullable String[] projection,
                                          @Nullable String sortOrder) {

        Cursor cursor = context.getContentResolver().query(uri,
                projection, // Which columns to return
                null,       // Which rows to return (all rows)
                null,       // Selection arguments (none)
                sortOrder); // Ordering

        return cursor;
    }

    /**
     * Convert number in byte size into readable format size as KB, MB, GB.
     *
     * @param size in bytes.
     * @return result formatted.
     */
    public static String formatBytesInReadableSize(Context context, long size) {
        return Formatter.formatShortFileSize(context, size);
    }

    /**
     * Get the name of device.
     *
     * @return the name of device.
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        if (model.startsWith(manufacturer)) {
            Log.i("InfoSistema", model.toUpperCase());
            return model.toUpperCase();
        } else {
            Log.i("InfoSistema", manufacturer.toUpperCase() + " " + model);
            return manufacturer.toUpperCase() + " " + model;
        }
    }

    //-------------------------------------- Contact Methods -------------------------------

    /**
     * Metodo que devuelve el directorio privado de la aplicación donde esta guardado el archivo cuyo nombre recibe por parametro
     *
     * @return
     */
    public static String getExternalVCFFileDirectory(Context context, String fileName) {
        String state = Environment.getExternalStorageState();
        String fileLocationValue = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileLocationValue = context.getExternalFilesDir(null).toString() + File.separator + fileName;
        } else {
            showToast(context, "You can not create the contact file.");
        }
        return fileLocationValue;
    }

    /**
     * Metodo temporal (obteniendo el archivo del directorio externo publico)
     *
     * @param context
     * @param fileName
     * @return
     */
    //TODO: Eliminar Este metodo al final
    public static String getExternalVCFFilePublicDirectory(Context context, String fileName) {
        String state = Environment.getExternalStorageState();
        String fileLocationValue = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileLocationValue = Environment.getExternalStorageDirectory().toString() + File.separator + fileName;
        }
        return fileLocationValue;
    }

    /**
     * Metodo que devuelve el archivo cuyo nombre recibe por parametro
     *
     * @param fileName
     * @return
     */
    public static File getExternalVCFFile(Context context, String fileName) {
        String state = Environment.getExternalStorageState();
        File file = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            file = new File(context.getExternalFilesDir(null).toString() + File.separator + fileName);
        } else {
            showToast(context, "The restore file does not exist.");
        }
        return file;
    }

    /**
     * Get cursor.
     *
     * @param context
     * @param uri             URI
     * @param projection      Which columns to return
     * @param selectionClause Which rows to return
     * @param sortOrder       ORDER BY
     * @return
     */
    public static Cursor getContactCursorFromUri(Context context, @NonNull Uri uri, @Nullable String[] projection,
                                                 @Nullable String selectionClause, @Nullable String sortOrder) {

        Cursor cursor = context.getContentResolver().query(
                uri,
                projection,
                selectionClause,
                null,
                sortOrder);

        return cursor;
    }
}