package org.escoladeltreball.freebackup.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Utils for Shared Preferences.
 *
 * Created by Arthur on 4/5/16.
 */
public class UtilsPreferences {

    /**
     * Save String on Shared Preferences.
     *
     * @param context the context.
     * @param key key for save value.
     * @param value value for save.
     */
    public static void setString(Context context, String key, String value) {

        SharedPreferences preferences = context.getSharedPreferences(Codes.SHARED_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();

        if (!key.isEmpty() & !value.isEmpty()) {
            editor.putString(key, value);
            editor.apply();
        }
    }


    /**
     * Get String from Shared Preferences.
     *
     * @param context the context.
     * @param keyToSearch key of value to search.
     * @param defaultValue value for default if keyToSearch not exists.
     * @return
     */
    public static String getString(Context context, String keyToSearch, String defaultValue) {

        // Get Shared Preferences.
        SharedPreferences preferences = context.getSharedPreferences(Codes.SHARED_PREFERENCES_NAME, 0);

        return preferences.getString(keyToSearch, defaultValue);
    }

    /**
     * Get Boolean from Shared Preferences.
     *
     * @param context the context.
     * @param keyToSearch key of value to search.
     * @param defaultValue value for default if keyToSearch not exists.
     * @return
     */
    public static Boolean getBoolean(Context context, String keyToSearch, Boolean defaultValue) {

        SharedPreferences preferences = context.getSharedPreferences(Codes.SHARED_PREFERENCES_NAME, 0);

        return preferences.getBoolean(keyToSearch, defaultValue);
    }

    /**
     * Set Boolean on Shared Preferences.
     *
     * @param context the context.
     * @param key key for save value.
     * @param value value for save.
     */
    public static void setBoolean(Context context, String key, Boolean value) {
        SharedPreferences settings = context.getSharedPreferences(Codes.SHARED_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        if (!key.isEmpty()) {
            editor.putBoolean(key, value);
            editor.apply();
        }
    }
}
