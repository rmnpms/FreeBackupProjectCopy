package org.escoladeltreball.freebackup.utils;

import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;

import org.escoladeltreball.freebackup.model.Contact;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by boty on 27/04/16.
 */
public class UtilsContacts {

    static ArrayList<String> contactsId = new ArrayList<>();

    static ArrayList<Contact> contactsList = new ArrayList<>();
    public static RecyclerView.Adapter adapter;

    private static Uri CONTACTS_QUERY_URI = ContactsContract.Contacts.CONTENT_URI;
    private static String CONTACTS_CONTACT_ID = ContactsContract.Contacts._ID;

    private static Uri PHONE_QUERY_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;


    //----------------------------------------------------- Generic Methods ---------------------------------------------------------------//

    /**
     * Metodo que devuelve el directorio donde esta guardado el archivo cuyo nombre recibe por parametro
     *
     * @return
     */
    public static String getExternalVCFFileDirectory(String fileName) {
        String state = Environment.getExternalStorageState();
        String fileLocationValue = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileLocationValue = Environment.getExternalStorageDirectory().toString() + File.separator + fileName;
        }
        return fileLocationValue;
    }

    /**
     * Metodo que devuelve el archivo cuyo nombre recibe por parametro
     *
     * @param fileName
     * @return
     */
    public static File getExternalVCFFile(String fileName) {
        String state = Environment.getExternalStorageState();
        File file = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + fileName);
        }
        return file;
    }
}
