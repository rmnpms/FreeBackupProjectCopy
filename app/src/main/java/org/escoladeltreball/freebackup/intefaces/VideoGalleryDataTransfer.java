package org.escoladeltreball.freebackup.intefaces;

/**
 * For transfer info from Adapter to Activity.
 *
 * Created by Arthur on 16/5/16.
 */
public interface VideoGalleryDataTransfer {

    void setTheNumberOfVideosSelected(Integer numberOfVideosSelected);

    void setCheckedTheGeneralCheckBox(boolean checked);
}