package org.escoladeltreball.freebackup.intefaces;

/**
 * For transfer info from Adapters to Activities.
 *
 * Created by Arthur on 15/5/16.
 */
public interface ImageGalleryDataTransfer {

    void setTheNumberOfImagesSelected(Integer numberOfImagesSelected);

    void setCheckedTheGeneralCheckBox(boolean checked);
}
