package org.escoladeltreball.freebackup.model;

/**
 * Model of Video.
 *
 * Created by Arthur on 16/5/16.
 */
public class Video {

    private boolean selected = true;
    private String title;
    private String mimeType;
    private String pathFile;
    private String bucketName;
    private int size;

    /**
     * Standard constructor.
     */
    public Video() {
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


}
