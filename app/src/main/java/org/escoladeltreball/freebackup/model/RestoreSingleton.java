package org.escoladeltreball.freebackup.model;

import java.util.ArrayList;

/**
 * Created by boty on 15/05/16.
 */
public class RestoreSingleton {

    private static RestoreSingleton instance;

    private ArrayList<String> contactListOfId;
    private ArrayList<Contact> contactsList;


    /**
     * Standard constructor for a Singleton Restore.
     */
    private RestoreSingleton() {

    }

    /**
     * Get an unique instance of Restore.
     *
     * @return an unique instance of Restore.
     */
    public static synchronized RestoreSingleton getInstance() {
        if (instance == null) {
            instance = new RestoreSingleton();
        }
        return instance;
    }

    //-----------------------------------CONTACTS------------------------------------------------

    /**
     * Get contact list of id
     *
     * @return
     */
    public ArrayList<String> getContactListOfId() {
        return contactListOfId;
    }

    /**
     * Set contact list of id
     *
     * @param contactListOfId
     */
    public void setContactListOfId(ArrayList<String> contactListOfId) {
        this.contactListOfId = contactListOfId;
    }

    /**
     * Get Contact list
     *
     * @return contactsList
     */
    public ArrayList<Contact> getContactsList() {
        return contactsList;
    }

    /**
     * Set contact List
     *
     * @param contactsList
     */
    public void setContactsList(ArrayList<Contact> contactsList) {
        this.contactsList = contactsList;
    }

    //-------------------------------------IMAGES------------------------------------------------









    //-------------------------------------VIDEOS------------------------------------------------










}
