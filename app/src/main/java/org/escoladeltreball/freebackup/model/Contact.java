package org.escoladeltreball.freebackup.model;

/**
 * Created by boty on 28/04/16.
 */
public class Contact{

    private String contactId;
    private String contactName;
    private String contactMobile = "";
    private String contactMail = "";
    private String contactOthers = "";
    private boolean selected = true;

    public Contact() {
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public String getContactOthers() {
        return contactOthers;
    }

    public void setContactOthers(String contactOthers) {
        this.contactOthers = contactOthers;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}