package org.escoladeltreball.freebackup.model;

import java.util.ArrayList;

/**
 * Model of Backup (Singleton).
 *
 * Created by Arthur on 5/5/16.
 */
public class BackupSingleton {

    private static BackupSingleton instance;

    private ArrayList<Image> imagesList;
    private ArrayList<String> pathOfImagesSelected;
    private int imagesTotalSelected;
    private long imagesTotalSize;

    private ArrayList<Video> videosList;
    private int videosTotalSelected;
    private long videosTotalSize;

    private ArrayList<String> contactListOfId;
    private ArrayList<Contact> contactsList;

    /**
     * Standard constructor for a Singleton Backup.
     */
    private BackupSingleton() {
    }

    /**
     * Get an unique instance of Backup.
     *
     * @return an unique instance of Backup.
     */
    public static synchronized BackupSingleton getInstance() {
        if (instance == null) {
            instance = new BackupSingleton();
        }
        return instance;
    }


    //-------------------------------------IMAGES------------------------------------------------

    /**
     * Get image list.
     *
     * @return the image list.
     */
    public ArrayList<Image> getImagesList() {
        return imagesList;
    }

    /**
     * Set image list.
     *
     * @param imagesList the image list.
     */
    public void setImagesList(ArrayList<Image> imagesList) {
        this.imagesList = imagesList;
    }


    public ArrayList<String> getPathOfImagesSelected() {
        return pathOfImagesSelected;
    }

    public void setPathOfImagesSelected(ArrayList<String> pathOfImagesSelected) {
        this.pathOfImagesSelected = pathOfImagesSelected;
    }

    /**
     * get total images selected.
     *
     * @return the number of images selected.
     */
    public int getImagesTotalSelected() {
        return imagesTotalSelected;
    }

    /**
     * Set the total of images selected.
     *
     * @param imagesTotalSelected the number of images selected.
     */
    public void setImagesTotalSelected(int imagesTotalSelected) {
        this.imagesTotalSelected = imagesTotalSelected;
    }

    /**
     * Get total size of images.
     *
     * @return the total size in bytes.
     */
    public long getImagesTotalSize() {
        return imagesTotalSize;
    }

    /**
     * Set total size of images.
     *
     * @param imagesTotalSize the total size of images.
     */
    public void setImagesTotalSize(long imagesTotalSize) {
        this.imagesTotalSize = imagesTotalSize;
    }


    //------------------------------------VIDEOS-------------------------------------------------

    /**
     * Get video list.
     *
     * @return the video list.
     */
    public ArrayList<Video> getVideosList() {
        return videosList;
    }

    /**
     * Set video list.
     *
     * @param videosList the video list.
     */
    public void setVideosList(ArrayList<Video> videosList) {
        this.videosList = videosList;
    }

    /**
     * get total videos selected.
     *
     * @return the number of videos selected.
     */
    public int getVideosTotalSelected() {
        return videosTotalSelected;
    }

    /**
     * Set the total of videos selected.
     *
     * @param videosTotalSelected the number of videos selected.
     */
    public void setVideosTotalSelected(int videosTotalSelected) {
        this.videosTotalSelected = videosTotalSelected;
    }

    /**
     * Get total size of videos.
     *
     * @return the total size in bytes.
     */
    public long getVideosTotalSize() {
        return videosTotalSize;
    }

    /**
     * Set total size of videos.
     *
     * @param videosTotalSize the total size of images.
     */
    public void setVideosTotalSize(long videosTotalSize) {
        this.videosTotalSize = videosTotalSize;
    }


    //-----------------------------------CONTACTS------------------------------------------------

    /**
     * Get contact list of id
     *
     * @return
     */
    public ArrayList<String> getContactListOfId() {
        return contactListOfId;
    }

    /**
     * Set contact list of id
     *
     * @param contactListOfId
     */
    public void setContactListOfId(ArrayList<String> contactListOfId) {
        this.contactListOfId = contactListOfId;
    }

    /**
     * Get Contact list
     * @return contactsList
     */
    public ArrayList<Contact> getContactsList() {
        return contactsList;
    }

    /**
     * Set contact List
     * @param contactsList
     */
    public void setContactsList(ArrayList<Contact> contactsList) {
        this.contactsList = contactsList;
    }


}