package org.escoladeltreball.freebackup.services;

import android.util.Log;

import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.IOException;
import java.util.Collections;

/**
 * Created by ramon on 17/05/16.
 */
public class CreateFolders {

    public static final String MAIN_FOLDER = "FreeBackup";
    public static final String[] DATA_FOLDERS = {"Images", "Videos", "Contacts"};

    public static void createFreeBackupFolder() throws IOException {

        String pageToken = null;
        boolean found = false;

        // Empty trash to avoid problems finding any existing file or folder
        ManageDriveConnectionActivity.mDriveService.files().emptyTrash().execute();

        do {
            FileList result = ManageDriveConnectionActivity.mDriveService.files().list()
                    .setQ("mimeType='application/vnd.google-apps.folder'")
                    .setFields("nextPageToken, files(id, name)")
                    .setPageToken(pageToken)
                    .execute();
            for (File file : result.getFiles()) {
                if (file.getName().equals(MAIN_FOLDER)) {
                    found = true;
                    Log.i("Estado FreeBackup", "La carpeta existe");
                    break;
                }
            }
            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        if (!found) {
            File fileMetadata = new File();
            fileMetadata.setName(MAIN_FOLDER);
            fileMetadata.setMimeType("application/vnd.google-apps.folder");

            ManageDriveConnectionActivity.mDriveService.files().create(fileMetadata)
                    .execute();
            Log.i("Estado FreeBackup", "Carpeta creada con éxito");
        }
    }

    public static File createFolder(String name) throws IOException {

        String id = null;
        String pageToken = null;
        boolean found = false;
        File folder = null;

        do {
            FileList result = null;
            try {
                result = ManageDriveConnectionActivity.mDriveService.files().list()
                        .setQ("mimeType='application/vnd.google-apps.folder'")
                        .setFields("nextPageToken, files(id, name)")
                        .setPageToken(pageToken)
                        .execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert result != null;
            for (File file : result.getFiles()) {
                if (file.getName().equals(name)) {
                    found = true;
                    Log.i("Estado Carpeta", "La carpeta existe");
                    break;
                }
            }
            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        if (!found) {
            try {
                FileList fileList = ManageDriveConnectionActivity
                        .mDriveService.files().list().execute();
                for (File file : fileList.getFiles()) {
                    String fileName = file.getName();
                    if (fileName.equals(CreateFolders.MAIN_FOLDER)) {
                        id = file.getId();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            File fileMetadata = new File();
            fileMetadata.setName(name);
            fileMetadata.setParents(Collections.singletonList(id));
            fileMetadata.setMimeType("application/vnd.google-apps.folder");

            folder = ManageDriveConnectionActivity.mDriveService.files().create(fileMetadata)
                    .execute();
            Log.i("Estado Carpeta", "Carpeta creada con éxito");

        }
        return folder;
    }

    public static void createDataFolders() throws IOException {
        for (String dataFolderName : DATA_FOLDERS) {
            createFolder(dataFolderName);
        }
    }
}




