package org.escoladeltreball.freebackup.services;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import org.escoladeltreball.freebackup.ui.activity.HomeActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ManageDriveConnectionActivity extends AppCompatActivity {

    private static final int REQUEST_ACCOUNT_PICKER = 1;
    private static final int REQUEST_AUTHORIZATION = 2;

    private static GoogleAccountCredential mCredential;
    private List<File> mFileList;
    private static Handler mHandler;
    public static Intent authIntent;
    public static Drive mDriveService;

    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIntent = new Intent(this, HomeActivity.class);
        mHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupDriveService();
    }

    public void setupDriveService() {
        mCredential = GoogleAccountCredential.usingOAuth2(this, Arrays.asList(DriveScopes.DRIVE));
        if (mDriveService == null) {
            String accountName = getSavedAccountName();
            if (accountName.isEmpty()) {
                startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            } else {
                mCredential.setSelectedAccountName(accountName);
                mDriveService = getDriveService();
                callDriveApi();
                startActivity(mIntent);
            }
        }
    }

    public void callDriveApi() {

        (new Thread(new Runnable() {
            @Override
            public void run() {

                mFileList = new ArrayList<>();

                mFileList.clear();
                try {

                    Drive.Files.List request = ManageDriveConnectionActivity.mDriveService.files().list();

                    request.setQ("mimeType = 'plain/text'");

                    do {
                        FileList files = request.execute();
                        mFileList.addAll(files.getFiles());
                        request.setPageToken(files.getNextPageToken());
                    } while (request.getPageToken() != null
                            && request.getPageToken().length() > 0);

                } catch (UserRecoverableAuthIOException e) {
                    authIntent = e.getIntent();
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            startActivityForResult(authIntent, REQUEST_AUTHORIZATION);
                        }
                    });
                } catch (GoogleAuthIOException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        })).start();
    }

    public static Drive getDriveService() {
        return new Drive.Builder(AndroidHttp.newCompatibleTransport(),
                new GsonFactory(), mCredential).build();
    }

    private String getSavedAccountName() {
        SharedPreferences prefs = getSharedPreferences("prefs_name", Context.MODE_PRIVATE);
        return prefs.getString("key_account_name", "");
    }

    public void saveAccountName(final String accountName) {
        SharedPreferences prefs = getSharedPreferences("prefs_name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("key_account_name", accountName);
        editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                if ((resultCode == RESULT_OK) && (data != null)) {

                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);
                        mDriveService = getDriveService();

                        callDriveApi();
                        saveAccountName(accountName);
                        startActivity(mIntent);
                    }
                }
                break;

            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    callDriveApi();
                    startActivity(mIntent);
                } else {

                }
                break;
        }
    }
}
