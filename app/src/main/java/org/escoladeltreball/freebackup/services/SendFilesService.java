package org.escoladeltreball.freebackup.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Image;
import org.escoladeltreball.freebackup.model.Video;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class SendFilesService extends IntentService {

    private static String id;

    public SendFilesService() {
        super("SendFilesService");
    }

    /* Método de la interfaz OnHandleIntentque recoge el intent enviado por una
     Activity como parámetro e inicia un servicio que él mismo se encarga de gestionar
     un único hilo. */
    @Override
    protected void onHandleIntent(final Intent intent) {
        try {
            CreateFolders.createFreeBackupFolder();
            CreateFolders.createDataFolders();
            sendFilesToFolders();
            sendNotification();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendFilesToFolders() throws IOException {

        for (String folderName : CreateFolders.DATA_FOLDERS) {

            try {
                FileList fileList = ManageDriveConnectionActivity.mDriveService
                        .files().list().execute();
                for (File file : fileList.getFiles()) {
                    String name = file.getName();
                    if (name.equals(folderName)) {
                        id = file.getId();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            switch (folderName) {

                case "Images":
                    ArrayList<Image> imageList = BackupSingleton.getInstance().getImagesList();


                    if (imageList.size() > 0) {
                        FileList fileListFromDrive = ManageDriveConnectionActivity
                                .mDriveService.files().list()
                                .setQ("name = 'Images'")
                                .execute();

                        for (File file : fileListFromDrive.getFiles()) {
                            ManageDriveConnectionActivity.mDriveService.files()
                                    .delete(file.getId())
                                    .execute();
                        }

                        File folder = CreateFolders.createFolder("Images");

                        for (int j = 0; j < imageList.size(); j++) {

                            File fileMetadata = new File();
                            fileMetadata.setName(imageList.get(j).getTitle());
                            fileMetadata.setParents(Collections.singletonList(folder.getId()));
                            java.io.File filePath = new java.io.File(imageList.get(j).getPathFile());
                            FileContent mediaContent = new FileContent("image/jpeg", filePath);
                            ManageDriveConnectionActivity.mDriveService.files()
                                    .create(fileMetadata, mediaContent)
                                    .setFields("id, parents")
                                    .execute();
                        }
                        Log.i("Carpeta", fileListFromDrive.getFiles().toString());
                        Log.i("Estado", "Subido con éxito");
                    }



                    break;
                case "Video":

                    ArrayList<Video> videoList = BackupSingleton.getInstance().getVideosList();


                    if (videoList.size() > 0) {
                        FileList fileListFromDrive = ManageDriveConnectionActivity
                                .mDriveService.files().list()
                                .setQ("name = 'Images'")
                                .execute();

                        for (File file : fileListFromDrive.getFiles()) {
                            ManageDriveConnectionActivity.mDriveService.files()
                                    .delete(file.getId())
                                    .execute();
                        }

                        File folder = CreateFolders.createFolder("Videos");

                        for (int j = 0; j < videoList.size(); j++) {

                            File fileMetadata = new File();
                            fileMetadata.setName(videoList.get(j).getTitle());
                            fileMetadata.setParents(Collections.singletonList(folder.getId()));
                            java.io.File filePath = new java.io.File(videoList.get(j).getPathFile());
                            FileContent mediaContent = new FileContent("image/jpeg", filePath);
                            ManageDriveConnectionActivity.mDriveService.files()
                                    .create(fileMetadata, mediaContent)
                                    .setFields("id, parents")
                                    .execute();
                        }
                        Log.i("Carpeta", fileListFromDrive.getFiles().toString());
                        Log.i("Estado", "Subido con éxito");
                    }

//                    ArrayList<Video> videoList = BackupSingleton.getInstance().getVideosList();
//                    for (int j = 0; j < videoList.size(); j++) {
//
//                        try {
//
//                            File fileMetadata = new File();
//                            fileMetadata.setName(videoList.get(j).getTitle());
//                            fileMetadata.setParents(Collections.singletonList(id));
//                            java.io.File filePath = new java.io.File(videoList.get(j).getPathFile());
//
//                            FileContent mediaContent = new FileContent("image/jpeg", filePath);
//
//                            ManageDriveConnectionActivity.mDriveService.files()
//                                    .create(fileMetadata, mediaContent)
//                                    .setFields("id, parents")
//                                    .execute();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        Log.i("Estado", "Subido con éxito");
//                    }
            }

        }
    }

    private void sendNotification() {



        // Uri with the sound for the notification
        Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(this)
                .setTicker(getResources().getString(R.string.content_title))
                .setSmallIcon(R.mipmap.ic_backup_white_24dp)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setSound(notificationSoundUri)
                .setContentTitle(getResources().getString(R.string.content_title))
                .setContentText(getResources().getString(R.string.content_text))
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        //Se esconde la notificación tras ser seleccionada
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }
}
