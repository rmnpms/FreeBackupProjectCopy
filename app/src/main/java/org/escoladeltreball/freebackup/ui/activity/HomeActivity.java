package org.escoladeltreball.freebackup.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.ui.presenter.HomePresenter;
import org.escoladeltreball.freebackup.services.SendFilesService;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Home.
 * <p/>
 * Created by Arthur on 25/4/16.
 */
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST = 0;
    private static final int CONTACT_REQUEST = 1;

    @Bind(R.id.toolbar_home_activity)
    Toolbar mToolbar;

    @Bind(R.id.nav_view)
    NavigationView mNavView;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.images_card_view_number_of_files)
    TextView mImagesNumberOfFiles;

    @Bind(R.id.images_card_view_size_files)
    TextView mImagesSizeFiles;

    @Bind(R.id.videos_card_view_number_of_files)
    TextView mVideosNumberOfFiles;

    @Bind(R.id.videos_card_view_size_files)
    TextView mVideosSizeFiles;

    @Bind(R.id.contacts_card_view_number_of_contacts)
    TextView mContactsNumber;

    private HomePresenter presenter;

    //TODO: crear un string para distinguir el tipo de operacón (import/export)
    String operation = "export";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        presenter = new HomePresenter(this);

        setUpView();
        showDataOfDevice();

        Log.d("PRUEBA", "-->"+ BackupSingleton.getInstance().getPathOfImagesSelected().size());


    }

    /**
     * Set up the View.
     */
    private void setUpView() {
        setUpToolbar();
        setUpNavigationView();
    }

    /**
     * Set up the Toolbar.
     */
    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /**
     * Set up the NavigationView.
     */
    private void setUpNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                mDrawerLayout,
                mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavView.setNavigationItemSelectedListener(this);
    }

    /**
     * Show info of Contacts, Images and Videos.
     */
    private void showDataOfDevice() {
        presenter.loadImagesInfoForShow();
        presenter.loadContactsInfoForShow(operation);
        presenter.loadVideosInfoForShow();
        //TODO: load videos    (presenter.loadDataOfVideos();)

    }

    /**
     * Set up (number of images, total size in bytes) of images on View.
     *
     * @param numberOfImages    number of images.
     * @param totalSizeOfImages total size of images.
     */
    public void setUpDataOfImages(String numberOfImages, String totalSizeOfImages) {
        mImagesNumberOfFiles.setText(numberOfImages);

        if (!totalSizeOfImages.isEmpty()) {
            mImagesSizeFiles.setText(getString(R.string.parenthesis_with_arg, totalSizeOfImages));
        } else {
            mImagesSizeFiles.setText("");
        }

    }

    /**
     * Set up (number of videos, total size in bytes) of videos on View.
     *
     * @param numberOfVideos    number of images.
     * @param totalSizeOfVideos total size of images.
     */
    public void setUpDataOfVideos(String numberOfVideos, String totalSizeOfVideos) {
        mVideosNumberOfFiles.setText(numberOfVideos);

        if (!totalSizeOfVideos.isEmpty()) {
            mVideosSizeFiles.setText(getString(R.string.parenthesis_with_arg, totalSizeOfVideos));
        } else {
            mVideosSizeFiles.setText("");
        }
    }

    /**
     * Set up the data of contacts number on view.
     *
     * @param numberOfContacts
     */
    public void setUpDataOfContacts(String numberOfContacts) {

        mContactsNumber.setText(numberOfContacts);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_device, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cloud) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_device) {

        } else if (id == R.id.nav_cloud) {

        } else if (id == R.id.nav_auto_backup) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_help) {

        } else if (id == R.id.nav_about_us) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @OnClick(R.id.images_card_view)
    public void onClickImagesCard() {
        launchActivity(ImageGalleryActivity.class);
    }

    @OnClick(R.id.videos_card_view)
    public void onClickVideosCard() {
        launchActivity(VideoGalleryActivity.class);
    }

    @OnClick(R.id.contacts_card_view)
    public void onClickContacts() {
        launchContactsActivity(ContactActivity.class, operation);
    }


    /**
     * Launch a new Activity.
     *
     * @param destinationClass the destination Activity.
     */
    public void launchActivity(Class<?> destinationClass) {
        Intent intent = new Intent(this, destinationClass);
        startActivityForResult(intent, REQUEST);

    }

    public void launchContactsActivity(Class<?> destinationClass, String operation) {
        Intent intent = new Intent(this, destinationClass);
        intent.putExtra("operation", operation);
        startActivityForResult(intent, CONTACT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST) {

            switch (resultCode) {
                case RESULT_OK:
                    presenter.updateImagesBackup();
                    presenter.updateVideosBackup();
                    break;

                case RESULT_CANCELED:
                    // Cancelación o cualquier situación de error.
                    break;
            }
        }
        else if (requestCode == CONTACT_REQUEST) {

            switch (resultCode) {
                case RESULT_OK:
                    String operationResult = data.getStringExtra("operation");
                    presenter.updateContactsListOfId(operationResult);
                    break;
            }
        }

    }

    @OnClick(R.id.main_action_buttom)
    public void onClick() {
//        if (operation.equals("export")) {
//            presenter.createTheVcfFileToExport();
//        } else {
//            presenter.importSelectedContacts();
//        }
        Intent service = new Intent(this, SendFilesService.class);
        startService(service);
    }
}
