package org.escoladeltreball.freebackup.ui.presenter;

import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Video;
import org.escoladeltreball.freebackup.ui.activity.VideoGalleryActivity;

import java.util.ArrayList;

/**
 * Video Presenter.
 *
 * Created by Arthur on 16/5/16.
 */
public class VideoGalleryPresenter {

    private VideoGalleryActivity view;

    public VideoGalleryPresenter(VideoGalleryActivity view) {
        this.view = view;
    }

    /**
     * Get Video files from Backup Singleton Object.
     */
    public void getAllMediaFiles() {
        //TODO: mostrar ProgresDialog
        ArrayList<Video> videos = BackupSingleton.getInstance().getVideosList();
        view.loadMediaFilesOnAdapter(videos);
    }


    /**
     * Set all videos of list as selected or unselected.
     *
     * @param state the state for set all videos as selected or not.
     */
    public void setAllListAsSelected(boolean state) {

        int videoListSize = BackupSingleton.getInstance().getVideosList().size();

        for (int i = 0; i < videoListSize; i++) {
            if (BackupSingleton.getInstance().getVideosList().get(i).isSelected() != state) {
                BackupSingleton.getInstance().getVideosList().get(i).setSelected(state);
            }
        }
    }

}
