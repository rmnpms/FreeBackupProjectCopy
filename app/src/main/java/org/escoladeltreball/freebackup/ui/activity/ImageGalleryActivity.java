package org.escoladeltreball.freebackup.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Image;
import org.escoladeltreball.freebackup.ui.adapter.ImageGalleryAdapter;
import org.escoladeltreball.freebackup.intefaces.ImageGalleryDataTransfer;
import org.escoladeltreball.freebackup.ui.presenter.ImageGalleryPresenter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * ImageGallery Activity.
 *
 * Created by Arthur on 7/5/16.
 */
public class ImageGalleryActivity extends AppCompatActivity implements ImageGalleryDataTransfer {

    @Bind(R.id.gallery_toolbar)
    Toolbar mGalleryToolbar;

    @Bind(R.id.gallery_toolbar_title)
    TextView mGalleryToolbarTitle;

    @Bind(R.id.gallery_recycler_view)
    RecyclerView mGalleryRecyclerView;

    @Bind(R.id.gallery_all_media_checkbox)
    AppCompatCheckBox mCheckedAllMedia;

    @Bind(R.id.numberOfMediaFilesSelected)
    TextView mNumberOfMediaFilesSelected;

    @Bind(R.id.numberOfAllMediaFiles)
    TextView mNumberOfAllMediaFiles;

    private ImageGalleryAdapter mAdapter;
    private ImageGalleryPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        presenter = new ImageGalleryPresenter(this);

        setUpView();


        presenter.getAllMediaFiles();
    }

    /**
     * Set up the View.
     */
    private void setUpView() {
        setUpToolbar();
        setUpInfoOfAllNumberOfImages();
        setUpInfoOfAllNumberOfImagesSelected();
    }

    /**
     * Set up the Toolbar.
     */
    private void setUpToolbar() {
        setSupportActionBar(mGalleryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mGalleryToolbarTitle.setText(getString(R.string.gallery_images_toolbar_title));

    }

    /**
     * Set up the total number of images.
     */
    private void setUpInfoOfAllNumberOfImages() {
        String numberOfAllImages = String.valueOf(BackupSingleton.getInstance().getImagesList().size());
        mNumberOfAllMediaFiles.setText(numberOfAllImages);
    }

    /**
     * Set up the total number of selected images.
     */
    private void setUpInfoOfAllNumberOfImagesSelected() {

        int numberOfImagesSelected = BackupSingleton.getInstance().getImagesTotalSelected();
        mNumberOfMediaFilesSelected.setText(String.valueOf(numberOfImagesSelected));

        if (numberOfImagesSelected == BackupSingleton.getInstance().getImagesList().size()) {
            mCheckedAllMedia.setChecked(true);
        }
        else {
            mCheckedAllMedia.setChecked(false);
        }
    }

    /**
     * Load Files on adapter.
     *
     * @param imagesList to load on adapter.
     */
    public void loadMediaFilesOnAdapter(ArrayList<Image> imagesList) {
        mAdapter = new ImageGalleryAdapter(imagesList, this);
        mGalleryRecyclerView.setAdapter(mAdapter);
        mGalleryRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mGalleryRecyclerView.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.gallery_confirm_buttom)
    public void onClickConfirm() {

        Log.d("PRUEBA", "confirm-->" + BackupSingleton.getInstance().getImagesTotalSelected());

        presenter.upDatePathOfImagesSelected();

        Intent intent = new Intent(this, HomeActivity.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.gallery_all_media_checkbox)
    public void onClickCheckBoxAllMedia() {

        presenter.setAllListAsSelected(mCheckedAllMedia.isChecked());

        mAdapter.notifyDataSetChanged();
        setUpInfoOfAllNumberOfImagesSelected();
    }

    @Override
    public void setTheNumberOfImagesSelected(Integer numberOfImagesSelected) {
        mNumberOfMediaFilesSelected.setText(String.valueOf(numberOfImagesSelected));
    }

    @Override
    public void setCheckedTheGeneralCheckBox(boolean checked) {
        if (checked) {
            mCheckedAllMedia.setChecked(true);
        }
        else {
            mCheckedAllMedia.setChecked(false);
        }
    }

    @Override
    public void onBackPressed() {
          super.onBackPressed();

        Log.d("PRUEBA", "back-->" + BackupSingleton.getInstance().getPathOfImagesSelected().size());

    }
}
