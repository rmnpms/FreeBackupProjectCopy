package org.escoladeltreball.freebackup.ui.adapter;

import android.net.Uri;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.intefaces.VideoGalleryDataTransfer;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Video;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * VideoGallery Adapter.
 *
 * Created by Arthur on 16/5/16.
 */
public class VideoGalleryAdapter extends RecyclerView.Adapter<VideoGalleryAdapter.VideoGalleryViewHolder>
        implements View.OnClickListener {

    private VideoGalleryDataTransfer mVideoGalleryDataTransferInterface;
    private ArrayList<Video> mVideosList;

    private View.OnClickListener listener;

    /**
     * VideoGalleryAdapter Constructor.
     *
     * @param videosList the list of videos to adapter.
     * @param videoGalleryDataTransferInterface the interface for transfer info from Adapter to Activity.
     */
    public VideoGalleryAdapter(ArrayList<Video> videosList,
                               VideoGalleryDataTransfer videoGalleryDataTransferInterface) {

        this.mVideosList = videosList;
        this.mVideoGalleryDataTransferInterface = videoGalleryDataTransferInterface;
    }

    @Override
    public VideoGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item, parent, false);

        VideoGalleryViewHolder videoGalleryViewHolder = new VideoGalleryViewHolder(view);

        videoGalleryViewHolder.galleryItemCheckBox.setOnClickListener(this);

        return videoGalleryViewHolder;
    }

    @Override
    public void onBindViewHolder(final VideoGalleryViewHolder videoGalleryViewHolder, final int position) {

        videoGalleryViewHolder.bindVideo(mVideosList.get(position));

        if (mVideosList.get(position).isSelected()) {
            videoGalleryViewHolder.galleryItemCheckBox.setChecked(true);
        }
        else {
            videoGalleryViewHolder.galleryItemCheckBox.setChecked(false);
        }

        videoGalleryViewHolder.galleryItemCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoGalleryViewHolder.galleryItemCheckBox.isChecked()) {

                    mVideosList.get(position).setSelected(true);
                    BackupSingleton.getInstance().getVideosList().get(position).setSelected(true);


                    int totalOfVideos = BackupSingleton.getInstance().getVideosList().size();
                    int totalVideosSelected = BackupSingleton.getInstance().getVideosTotalSelected();

                    BackupSingleton.getInstance().setVideosTotalSelected(totalVideosSelected + 1);

                    if (BackupSingleton.getInstance().getImagesTotalSelected() == totalOfVideos) {
                        mVideoGalleryDataTransferInterface.setCheckedTheGeneralCheckBox(true);
                    }

                    totalVideosSelected = BackupSingleton.getInstance().getVideosTotalSelected();
                    mVideoGalleryDataTransferInterface.setTheNumberOfVideosSelected(totalVideosSelected);
                }
                else {
                    mVideosList.get(position).setSelected(false);
                    BackupSingleton.getInstance().getVideosList().get(position).setSelected(false);


                    int totalVideosSelected = BackupSingleton.getInstance().getVideosTotalSelected();

                    BackupSingleton.getInstance().setVideosTotalSelected(totalVideosSelected - 1);

                    totalVideosSelected = BackupSingleton.getInstance().getVideosTotalSelected();
                    mVideoGalleryDataTransferInterface.setTheNumberOfVideosSelected(totalVideosSelected);
                    mVideoGalleryDataTransferInterface.setCheckedTheGeneralCheckBox(false);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mVideosList.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }

    public class VideoGalleryViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.gallery_item_image_view)
        ImageView galleryItemImageView;

        @Bind(R.id.gallery_item_check_box)
        AppCompatCheckBox galleryItemCheckBox;

        public VideoGalleryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindVideo(final Video video) {
            Glide.with(itemView.getContext())
                    .load(Uri.fromFile(new File(video.getPathFile())))
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(galleryItemImageView);
        }
    }

}
