package org.escoladeltreball.freebackup.ui.presenter;

import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Contact;
import org.escoladeltreball.freebackup.model.RestoreSingleton;
import org.escoladeltreball.freebackup.ui.activity.ContactActivity;

import java.util.ArrayList;

/**
 * Created by iam8355243 on 10/05/16.
 */
public class ContactPresenter {

    private ContactActivity view;

    public ContactPresenter(ContactActivity view) {
        this.view = view;
    }

    /**
     * Get contact list
     *
     * @return
     */
    public ArrayList<Contact> getContactsList() {

        ArrayList<Contact> contactList;

        //operation type (export/import)
        String operation = getOperationType();

        if (operation.equals("export")) {
            contactList = BackupSingleton.getInstance().getContactsList();
        } else {
            contactList = RestoreSingleton.getInstance().getContactsList();
        }
        return contactList;
    }

    /**
     * Get the source button from which you accessed the activity (export / import)
     *
     * @return
     */
    public String getOperationType() {

        String operation = view.getIntent().getExtras().getString("operation");

        return operation;
    }

    /**
     * Set the status of the general checkbox
     *
     * @param selectedContactsId
     * @param contacts
     * @return
     */
    public boolean chekedOrUnckeked(ArrayList<String> selectedContactsId, ArrayList<Contact> contacts) {

        int selectedContacts = selectedContactsId.size();
        int allContacts = contacts.size();

        return selectedContacts == allContacts;
    }

}
