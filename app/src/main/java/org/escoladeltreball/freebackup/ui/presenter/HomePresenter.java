package org.escoladeltreball.freebackup.ui.presenter;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;

import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Contact;
import org.escoladeltreball.freebackup.model.Image;
import org.escoladeltreball.freebackup.model.RestoreSingleton;
import org.escoladeltreball.freebackup.model.Video;
import org.escoladeltreball.freebackup.ui.activity.HomeActivity;
import org.escoladeltreball.freebackup.utils.Codes;
import org.escoladeltreball.freebackup.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.android.AndroidCustomFieldScribe;
import ezvcard.io.text.VCardReader;
import ezvcard.io.text.VCardWriter;
import ezvcard.util.StringUtils;

/**
 * Presenter of Home Activity.
 * <p/>
 * Created by Arthur on 2/5/16.
 */
public class HomePresenter {

    private HomeActivity view;

    public HomePresenter(HomeActivity view) {
        this.view = view;

        loadAllDataFromDevice();
    }

    //-------------------------LOAD DATA----------------------------------

    /**
     * Load all data (contacts, images, videos) from device.
     */
    private void loadAllDataFromDevice() {
        loadImagesFromDevice();
        loadVideosFromDevice();
        loadContactsFromDevice();
    }

    /**
     * Load all images from device.
     */
    private void loadImagesFromDevice() {
        BackupSingleton.getInstance().setImagesList(getAllImagesFromDevice());
        BackupSingleton.getInstance().setImagesTotalSize(getTotalSizeOfSelectedImages());
        BackupSingleton.getInstance().setPathOfImagesSelected(getPathsOfImagesSelected());
        BackupSingleton.getInstance().setImagesTotalSelected(getTotalNumberOfImagesSelected());

    }

    /**
     * Load all videos from device.
     */
    public void loadVideosFromDevice() {
        BackupSingleton.getInstance().setVideosList(getAllVideosFromDevice());
        BackupSingleton.getInstance().setVideosTotalSize(getTotalSizeOfSelectedVideos());
        BackupSingleton.getInstance().setVideosTotalSelected(getTotalNumberOfVideosSelected());
    }

    /**
     * Load all contacts from device and id of Selected Contacts
     */
    private void loadContactsFromDevice() {
        //--------- Backup
        BackupSingleton.getInstance().setContactsList(getAllContactsFromDevice());
        BackupSingleton.getInstance().setContactListOfId(getIdOfSelectedContacts("Backup"));
        //--------- Restore
        RestoreSingleton.getInstance().setContactsList(getAllContactsFromFile());
        RestoreSingleton.getInstance().setContactListOfId(getIdOfSelectedContacts("Restore"));
    }


    //-------------------------IMAGES----------------------------------

    /**
     * Get list of all images from device.
     *
     * @return list of images.
     */
    private ArrayList<Image> getAllImagesFromDevice() {

        ArrayList<Image> imagesList = new ArrayList<>();

        String[] projection = new String[]{
                MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.MIME_TYPE,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATA
        };

        Uri imagesUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        Cursor cursor = Utils.getCursorFromUri(view, imagesUri, projection, null);

        if (cursor.moveToFirst()) {

            // Get the index of columns.
            int titleColumn = cursor.getColumnIndex(MediaStore.Images.Media.TITLE);
            int bucketNameColumn = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            int mimeTypeColumn = cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE);
            int sizeColumn = cursor.getColumnIndex(MediaStore.Images.Media.SIZE);
            int dataPathColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

            String bucketName = cursor.getString(bucketNameColumn);

            do {
                if (!bucketName.equals(Codes.NAME_OF_THUMBNAILS_FOLDER)) {

                    Image image = new Image();

                    image.setBucketName(bucketName);
                    image.setMimeType(cursor.getString(mimeTypeColumn));
                    image.setPathFile(cursor.getString(dataPathColumn));
                    image.setSize(cursor.getInt(sizeColumn));
                    image.setTitle(cursor.getString(titleColumn));

                    imagesList.add(image);
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return imagesList;
    }

    /**
     * Get total size of all images.
     *
     * @return total size in bytes.
     */
    private long getTotalSizeOfSelectedImages() {

        long sizeOfAllImages = 0;
        ArrayList<Image> imageList = BackupSingleton.getInstance().getImagesList();

        if (!imageList.isEmpty()) {
            for (Image image : imageList) {
                if (image.isSelected()) {
                    sizeOfAllImages = sizeOfAllImages + image.getSize();
                }
            }
        }
        return sizeOfAllImages;
    }

    /**
     * Get the total number of images selected.
     * (by default all images of the list when init the app).
     *
     * @return the total number of images.
     */
    private int getTotalNumberOfImagesSelected() {

        return BackupSingleton.getInstance().getPathOfImagesSelected().size();
    }

    /**
     * -----------------------------------------
     */
    private ArrayList<String> getPathsOfImagesSelected() {

        ArrayList<String> pathsOfImagesSelected = new ArrayList<>();

        ArrayList<Image> imageList = BackupSingleton.getInstance().getImagesList();

        if (!imageList.isEmpty()) {
            for (Image image : imageList) {
                if (image.isSelected()) {
                    pathsOfImagesSelected.add(image.getPathFile());
                }
            }
        }
        return pathsOfImagesSelected;
    }


    /**
     * Load data of images.
     */
    public void loadImagesInfoForShow() {
        int imagesCount = 0;

        for (Image image : BackupSingleton.getInstance().getImagesList()) {
            if (image.isSelected()) {
                imagesCount++;
            }
        }

        String numberOfImages = String.valueOf(imagesCount);

        String totalSizeOfImages = "";

        if (BackupSingleton.getInstance().getImagesTotalSize() > 0) {
            totalSizeOfImages = Utils.formatBytesInReadableSize(view, BackupSingleton.getInstance().getImagesTotalSize());
        }
        view.setUpDataOfImages(numberOfImages, totalSizeOfImages);
    }


    //--------------------------VIDEOS-----------------------------------

    /**
     * Get list of all videos from device.
     *
     * @return list of videos.
     */
    private ArrayList<Video> getAllVideosFromDevice() {

        ArrayList<Video> videosList = new ArrayList<>();

        String[] projection = new String[]{
                MediaStore.Video.Media.MIME_TYPE,
                MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.DATA
        };

        Uri videosUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        Cursor cursor = Utils.getCursorFromUri(view, videosUri, projection, null);

        if (cursor.moveToFirst()) {

            // Get the index of columns.
            int mimeTypeColumn = cursor.getColumnIndex(MediaStore.Video.Media.MIME_TYPE);
            int sizeColumn = cursor.getColumnIndex(MediaStore.Video.Media.SIZE);
            int dataPathColumn = cursor.getColumnIndex(MediaStore.Video.Media.DATA);

            do {
                Video video = new Video();

                video.setMimeType(cursor.getString(mimeTypeColumn));
                video.setPathFile(cursor.getString(dataPathColumn));
                video.setSize(cursor.getInt(sizeColumn));

                videosList.add(video);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return videosList;
    }

    /**
     * Get total size of all videos.
     *
     * @return total size in bytes.
     */
    private long getTotalSizeOfSelectedVideos() {

        long sizeOfAllVideos = 0;
        ArrayList<Video> videoList = BackupSingleton.getInstance().getVideosList();

        if (!videoList.isEmpty()) {
            for (Video video : videoList) {
                if (video.isSelected()) {
                    sizeOfAllVideos = sizeOfAllVideos + video.getSize();
                }
            }
        }
        return sizeOfAllVideos;
    }

    /**
     * Get the total number of videos selected.
     * (by default all videos of the list when init the app).
     *
     * @return the total number of videos.
     */
    private int getTotalNumberOfVideosSelected() {
        return BackupSingleton.getInstance().getVideosList().size();
    }

    /**
     * Load data of videos.
     */
    public void loadVideosInfoForShow() {
        int videosCount = 0;

        for (Video video : BackupSingleton.getInstance().getVideosList()) {
            if (video.isSelected()) {
                videosCount++;
            }
        }

        String numberOfVideos = String.valueOf(videosCount);

        String totalSizeOfVideos = "";

        if (BackupSingleton.getInstance().getVideosTotalSize() > 0) {
            totalSizeOfVideos = Utils.formatBytesInReadableSize(view, BackupSingleton.getInstance().getVideosTotalSize());
        }
        view.setUpDataOfVideos(numberOfVideos, totalSizeOfVideos);
    }

    //-------------------------CONTACTS----------------------------------

    /**
     * Get id of selected contacts.
     *
     * @param operation
     * @return
     */
    private ArrayList<String> getIdOfSelectedContacts(String operation) {

        ArrayList<String> contactsId = new ArrayList<>();

        ArrayList<Contact> contacts;

        if (operation.equals("Backup")) {
            contacts = BackupSingleton.getInstance().getContactsList();
        } else {
            contacts = RestoreSingleton.getInstance().getContactsList();
        }

        for (Contact contact : contacts) {

            if (contact.isSelected()) {
                contactsId.add(contact.getContactId());
            }
        }
        return contactsId;
    }

    /**
     * Get list of all contacts from device.
     *
     * @return
     */
    private ArrayList<Contact> getAllContactsFromDevice() {

        final String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                PHONE_DISPLAY_NAME = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER,
                PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;

        Uri DATA_QUERY_URI = ContactsContract.Data.CONTENT_URI;

        Map<String, Contact> contactMap = new LinkedHashMap<>();

        Cursor cursor = view.getContentResolver().query(
                DATA_QUERY_URI,
                new String[]{PHONE_CONTACT_ID, PHONE_DISPLAY_NAME, PHONE_NUMBER, PHONE_TYPE},
                null,
                null,
                PHONE_DISPLAY_NAME);

        /*  Get the phone or email contact and keep it within the object contact   */
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                String idContact = cursor.getString(cursor.getColumnIndex(PHONE_CONTACT_ID));
                String nameContact = cursor.getString(cursor.getColumnIndex(PHONE_DISPLAY_NAME));
                String phone = cursor.getString(cursor.getColumnIndex(PHONE_NUMBER));
                String phoneType = cursor.getString(cursor.getColumnIndex(PHONE_TYPE));

                boolean hasMobile = phoneType != null && phone != null && phoneType.equals("2");
                boolean hasMail = phoneType != null && phone != null && phoneType.equals("3");
                boolean hasOthers = phoneType != null && phone != null && phoneType.equals("1");

                Contact contact;
                // keep all contacts
                if (!contactMap.containsKey(idContact)) {
                    contact = new Contact();
                    contact.setContactId(idContact);
                    contact.setContactName(nameContact);

                    contactMap.put(idContact, contact);

                }
                //if it is already in the hash we recover
                else {
                    contact = contactMap.get(idContact);
                }

                // Add mobile, mail and other (home, etc ..) in case you have
                if (hasMobile)
                    contact.setContactMobile(phone);
                if (hasMail)
                    contact.setContactMail(phone);
                if (hasOthers)
                    contact.setContactOthers(phone);
            }
        }
        ArrayList<Contact> contactsList = new ArrayList<Contact>(contactMap.values());
        return contactsList;
    }

    /**
     * Get list of all contacts from vcf file .
     *
     * @return
     */
    public ArrayList<Contact> getAllContactsFromFile() {

        ArrayList<Contact> contactsList = new ArrayList<>();

        String vcfFileContactsToImport = Codes.VCF_FILE_CONTACTS_TO_IMPORT;
        File file = Utils.getExternalVCFFile(view, vcfFileContactsToImport);

        if (file.exists()) {
            try {
                VCardReader reader = new VCardReader(file);
                reader.registerScribe(new AndroidCustomFieldScribe());
                Contact contact;
                int count = 0;

                List<VCard> vCards = Ezvcard.parse(file).register(new AndroidCustomFieldScribe()).all();    //obtenemos del archivo una lista de vcards

                for (VCard card : vCards) {

                    boolean hasName = card.getFormattedName().getValue() != null;
                    boolean hasPhone = (card.getTelephoneNumbers().size() > 0) && card.getTelephoneNumbers().get(0).getText() != null;
                    boolean hasMail = (card.getEmails().size() > 0) && card.getEmails().get(0).toString() != null;
                    boolean hasOthers = (card.getTelephoneNumbers().size() > 1) && card.getTelephoneNumbers().get(1).getText() != null && (!card.getTelephoneNumbers().get(1).getTypes().equals(card.getTelephoneNumbers().get(0).getTypes()));

                    contact = new Contact();
                    contact.setContactId(String.valueOf(count));    //The temporary identifier of the contract is the position within the file

                    //Control when fields they are null
                    if (hasName)
                        contact.setContactName(card.getFormattedName().getValue());

                    if (!hasName && hasMail && hasPhone)
                        contact.setContactName(card.getEmails().get(0).toString());

                    if (!hasName && !hasMail && hasPhone)
                        contact.setContactName(card.getTelephoneNumbers().get(0).getText());

                    if (hasPhone)
                        contact.setContactMobile(card.getTelephoneNumbers().get(0).getText());

                    if (hasMail)
                        contact.setContactMail(card.getEmails().get(0).toString());

                    if (hasOthers)
                        contact.setContactOthers(card.getTelephoneNumbers().get(1).getText());

                    contactsList.add(contact);

                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Contacts", file + "does not exist");
        }
        return contactsList;
    }

    /**
     * Load data of Contacts.
     */
    public void loadContactsInfoForShow(String operationResult) {

        String contactsNumber;

        if (operationResult.equals("export")) {
            //--------- Backup
            contactsNumber = String.valueOf(BackupSingleton.getInstance().getContactListOfId().size());

        } else {
            //--------- Restore
            contactsNumber = String.valueOf(RestoreSingleton.getInstance().getContactListOfId().size());
        }

        view.setUpDataOfContacts(contactsNumber);
    }


    //-------------------------UPDATE DATA--------------------------------

    /**
     * Update images info for show.
     */
    public void updateImagesBackup() {
        BackupSingleton.getInstance().setImagesTotalSize(getTotalSizeOfSelectedImages());
        loadImagesInfoForShow();
    }

    /**
     * Update videos info for show.
     */
    public void updateVideosBackup() {
        BackupSingleton.getInstance().setVideosTotalSize(getTotalSizeOfSelectedVideos());
        loadVideosInfoForShow();
    }

    /**
     * Update contact info for show
     *
     * @param operationResult
     */
    public void updateContactsListOfId(String operationResult) {
        loadContactsInfoForShow(operationResult);
    }


    //-----------------------------VCF-------------------------------------

    /**
     * Create the Vcf file, checking if they are exported all or selected
     */
    public void createTheVcfFileToExport() {

        String vcfFileToExport = Codes.VCF_FILE_TO_EXPORT;

        ArrayList<String> selectedContactsToExport = BackupSingleton.getInstance().getContactListOfId();
        ArrayList<Contact> allContacts = BackupSingleton.getInstance().getContactsList();

        String storage_path = Utils.getExternalVCFFileDirectory(view, vcfFileToExport);

        Uri CONTACTS_QUERY_URI = ContactsContract.Contacts.CONTENT_URI;
        String CONTACTS_CONTACT_ID = ContactsContract.Contacts._ID;

        StringBuilder sbListContacts;

        // If are not selected all
        if (selectedContactsToExport.size() < allContacts.size()) {

            if (selectedContactsToExport.size() > 0) {

                try {
                    FileOutputStream mFileOutputStream = new FileOutputStream(storage_path, false); //where we want to write data

                    // Condition: id's of contacts that are in the ArrayList
                    String contactsId = StringUtils.join(selectedContactsToExport, ", ");
                    String selectionClause = CONTACTS_CONTACT_ID + " IN ( " + contactsId + ")";

                    Cursor cursor = Utils.getContactCursorFromUri(view, CONTACTS_QUERY_URI, null, selectionClause, null);

                    sbListContacts = getVCardSBuilder(view, cursor);    //We get StringBuilder with all selected contacts

                    mFileOutputStream.write(sbListContacts.toString().getBytes());      //write contacts in the export file

                    Log.i("Contacts", "Exported Selected Contacts" + storage_path);

                } catch (IOException io) {
                    io.printStackTrace();
                }
            } else {
                Log.i("Contacts", "No contacts selected");
            }
        } else {

            try {
                Cursor cursor = Utils.getContactCursorFromUri(view, CONTACTS_QUERY_URI, null, null, null);

                FileOutputStream mFileOutputStream = new FileOutputStream(storage_path, false); ////where we want to write data

                sbListContacts = getVCardSBuilder(view, cursor);   //We get StringBuilder with all contacts from device

                mFileOutputStream.write(sbListContacts.toString().getBytes());    //write contacts in the export file

                Log.i("Contacts", "Exported all contacts" + storage_path);

            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }

    /**
     * Browse the cursor to get the contacts
     *
     * @param context
     * @param c
     * @return sb       a StringBuilder with the contacts
     */
    public static StringBuilder getVCardSBuilder(Context context, Cursor c) {

        StringBuilder sb = new StringBuilder();
        String vCardString = null;

        if (c.getCount() > 0) {

            while (c.moveToNext()) {

                String lookupKey = c.getString(c.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
                AssetFileDescriptor fd;
                try {
                    fd = context.getContentResolver().openAssetFileDescriptor(uri, "r");

                    FileInputStream fis = fd.createInputStream();
                    byte[] buf = new byte[(int) fd.getDeclaredLength()];
                    fis.read(buf);
                    vCardString = new String(buf);
                    sb.append(vCardString);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        c.close();
        return sb;
    }

    /**
     * Import selected contacts
     */
    public void importSelectedContacts() {

        String vcfFileContactsToImport = Codes.VCF_FILE_CONTACTS_TO_IMPORT;
        String vFileContactsToImportTemporary = Codes.VCF_FILE_TO_IMPORT_TEMP;

        ArrayList<String> selectedContactsImport = RestoreSingleton.getInstance().getContactListOfId();
        ArrayList<Contact> allContacts = RestoreSingleton.getInstance().getContactsList();

        // If are not selected all
        if (selectedContactsImport.size() < allContacts.size()) {

            File file = Utils.getExternalVCFFile(view, vcfFileContactsToImport);
            File fileImport = Utils.getExternalVCFFile(view, vFileContactsToImportTemporary);
            VCardWriter vCardWriter;

            try {
                List<VCard> vCards = Ezvcard.parse(file).register(new AndroidCustomFieldScribe()).all();

                vCardWriter = new VCardWriter(fileImport);
                vCardWriter.registerScribe(new AndroidCustomFieldScribe());

                // Get contacts with the id's (position in the file)
                for (int i = 0; i < selectedContactsImport.size(); i++) {

                    int contactPosition = Integer.parseInt(selectedContactsImport.get(i));
                    vCardWriter.write(vCards.get(contactPosition));   //write contacts in the export file
                }
                vCardWriter.close();

                //import contacts that are in the temporary file (contains the selected contacts)
                importContacts(view, vFileContactsToImportTemporary);

                Log.i("Contacts", "Import Selected Contact");

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            // import all contacts from original file
            importContacts(view, vcfFileContactsToImport);

            Log.i("Contacts", "Import All Contact");
        }
    }

    /**
     * Import contacts from a file that receives as parameter
     *
     * @param context
     * @param vFileContactsToImport
     */
    public void importContacts(Context context, String vFileContactsToImport) {

        String storageVcf = Utils.getExternalVCFFileDirectory(view, vFileContactsToImport);

        //TODO: descomentar esta parte para hacer la importacion del archivo temporal a la agenda(Contacts)
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setDataAndType(Uri.fromFile(new File(storageVcf)), "text/x-vcard");
//        context.startActivity(intent);
    }
}
