package org.escoladeltreball.freebackup.ui.adapter;

import android.net.Uri;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Image;
import org.escoladeltreball.freebackup.intefaces.ImageGalleryDataTransfer;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Gallery Adapter.
 *
 * Created by Arthur on 21/3/16.
 */
public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.GalleryViewHolder>
        implements View.OnClickListener {

    private ImageGalleryDataTransfer mImageGalleryDataTransferInterface;
    private ArrayList<Image> mImagesList;

    private View.OnClickListener listener;

    /**
     * ImageGalleryAdapter Constructor.
     *
     * @param imagesList the list of images to adapter.
     * @param imageGalleryDataTransferInterface the interface for transfer info from Adapters to Activities.
     */
    public ImageGalleryAdapter(ArrayList<Image> imagesList, ImageGalleryDataTransfer imageGalleryDataTransferInterface) {
        this.mImagesList = imagesList;
        this.mImageGalleryDataTransferInterface = imageGalleryDataTransferInterface;
    }


    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item, parent, false);

        GalleryViewHolder galleryViewHolder = new GalleryViewHolder(view);

        galleryViewHolder.galleryItemCheckBox.setOnClickListener(this);

        return galleryViewHolder;
    }

    @Override
    public void onBindViewHolder(final GalleryViewHolder galleryViewHolder, final int position) {

        galleryViewHolder.bindImage(mImagesList.get(position));

        if (mImagesList.get(position).isSelected()) {
            galleryViewHolder.galleryItemCheckBox.setChecked(true);
        }
        else {
            galleryViewHolder.galleryItemCheckBox.setChecked(false);
        }

        galleryViewHolder.galleryItemCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (galleryViewHolder.galleryItemCheckBox.isChecked()) {

                    mImagesList.get(position).setSelected(true);
                    BackupSingleton.getInstance().getImagesList().get(position).setSelected(true);


                    int totalOfImages = BackupSingleton.getInstance().getImagesList().size();
                    int totalImagesSelected = BackupSingleton.getInstance().getImagesTotalSelected();

                    BackupSingleton.getInstance().setImagesTotalSelected(totalImagesSelected + 1);

                    if (BackupSingleton.getInstance().getImagesTotalSelected() == totalOfImages) {
                        mImageGalleryDataTransferInterface.setCheckedTheGeneralCheckBox(true);
                    }

                    totalImagesSelected = BackupSingleton.getInstance().getImagesTotalSelected();
                    mImageGalleryDataTransferInterface.setTheNumberOfImagesSelected(totalImagesSelected);
                }
                else {
                    mImagesList.get(position).setSelected(false);
                    BackupSingleton.getInstance().getImagesList().get(position).setSelected(false);


                    int totalImagesSelected = BackupSingleton.getInstance().getImagesTotalSelected();

                    BackupSingleton.getInstance().setImagesTotalSelected(totalImagesSelected - 1);

                    totalImagesSelected = BackupSingleton.getInstance().getImagesTotalSelected();
                    mImageGalleryDataTransferInterface.setTheNumberOfImagesSelected(totalImagesSelected);
                    mImageGalleryDataTransferInterface.setCheckedTheGeneralCheckBox(false);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mImagesList.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.gallery_item_image_view)
        ImageView galleryItemImageView;

        @Bind(R.id.gallery_item_check_box)
        AppCompatCheckBox galleryItemCheckBox;

        public GalleryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindImage(final Image image) {
            Glide.with(itemView.getContext()).load(Uri.fromFile(new File(image.getPathFile())))
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(galleryItemImageView);
        }
    }

}
