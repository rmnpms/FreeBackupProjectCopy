package org.escoladeltreball.freebackup.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.intefaces.VideoGalleryDataTransfer;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Video;
import org.escoladeltreball.freebackup.ui.adapter.VideoGalleryAdapter;
import org.escoladeltreball.freebackup.ui.presenter.VideoGalleryPresenter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * VideoGallery Activity.
 *
 * Created by Arthur on 16/5/16.
 */
public class VideoGalleryActivity extends AppCompatActivity implements VideoGalleryDataTransfer {

    @Bind(R.id.gallery_toolbar)
    Toolbar mGalleryToolbar;

    @Bind(R.id.gallery_toolbar_title)
    TextView mGalleryToolbarTitle;

    @Bind(R.id.gallery_recycler_view)
    RecyclerView mGalleryRecyclerView;

    @Bind(R.id.gallery_all_media_checkbox)
    AppCompatCheckBox mCheckedAllMedia;

    @Bind(R.id.numberOfMediaFilesSelected)
    TextView mNumberOfMediaFilesSelected;

    @Bind(R.id.numberOfAllMediaFiles)
    TextView mNumberOfAllMediaFiles;

    private VideoGalleryAdapter mAdapter;
    private VideoGalleryPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        presenter = new VideoGalleryPresenter(this);

        setUpView();
        presenter.getAllMediaFiles();
    }

    /**
     * Set up the View.
     */
    private void setUpView() {
        setUpToolbar();
        setUpInfoOfAllNumberOfVideos();
        setUpInfoOfAllNumberOfVideosSelected();
    }

    /**
     * Set up the Toolbar.
     */
    private void setUpToolbar() {
        setSupportActionBar(mGalleryToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mGalleryToolbarTitle.setText(getString(R.string.gallery_videos_toolbar_title));

    }

    /**
     * Set up the total number of videos.
     */
    private void setUpInfoOfAllNumberOfVideos() {
        String numberOfAllVideos = String.valueOf(BackupSingleton.getInstance().getVideosList().size());
        mNumberOfAllMediaFiles.setText(numberOfAllVideos);
    }

    /**
     * Set up the total number of selected images.
     */
    private void setUpInfoOfAllNumberOfVideosSelected() {

        int numberOfVideosSelected = BackupSingleton.getInstance().getVideosTotalSelected();
        mNumberOfMediaFilesSelected.setText(String.valueOf(numberOfVideosSelected));

        if (numberOfVideosSelected == BackupSingleton.getInstance().getVideosList().size()) {
            mCheckedAllMedia.setChecked(true);
        }
        else {
            mCheckedAllMedia.setChecked(false);
        }
    }

    /**
     * Load Files on adapter.
     *
     * @param videoList to load on adapter.
     */
    public void loadMediaFilesOnAdapter(ArrayList<Video> videoList) {
        mAdapter = new VideoGalleryAdapter(videoList, this);
        mGalleryRecyclerView.setAdapter(mAdapter);
        mGalleryRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mGalleryRecyclerView.setHasFixedSize(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.gallery_confirm_buttom)
    public void onClickConfirm() {
        Intent intent = new Intent(this, HomeActivity.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.gallery_all_media_checkbox)
    public void onClickCheckBoxAllMedia() {

        if (mCheckedAllMedia.isChecked()) {

            int numberOfAllVideos = BackupSingleton.getInstance().getVideosList().size();
            BackupSingleton.getInstance().setVideosTotalSelected(numberOfAllVideos);

            presenter.setAllListAsSelected(true);
        }
        else {
            BackupSingleton.getInstance().setVideosTotalSelected(0);

            presenter.setAllListAsSelected(false);
        }

        mAdapter.notifyDataSetChanged();
        setUpInfoOfAllNumberOfVideosSelected();
    }

    @Override
    public void setTheNumberOfVideosSelected(Integer numberOfImagesSelected) {
        mNumberOfMediaFilesSelected.setText(String.valueOf(numberOfImagesSelected));
    }

    @Override
    public void setCheckedTheGeneralCheckBox(boolean checked) {
        if (checked) {
            mCheckedAllMedia.setChecked(true);
        }
        else {
            mCheckedAllMedia.setChecked(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

