package org.escoladeltreball.freebackup.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.intefaces.ContactDataTransfer;
import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Contact;
import org.escoladeltreball.freebackup.model.RestoreSingleton;
import org.escoladeltreball.freebackup.ui.adapter.ContactAdapter;
import org.escoladeltreball.freebackup.ui.presenter.ContactPresenter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by boty on 4/05/16.
 */
public class ContactActivity extends AppCompatActivity implements ContactDataTransfer {

    @Bind(R.id.recyclerview_contacts_activity)
    RecyclerView mContactsRecycle;

    @Bind(R.id.toolbar_contact_activity)
    Toolbar mToolbarContactActivity;

    public static TextView textSelectedContactsAllContacts;
    public static AppCompatCheckBox generalCheckbox;

    private ContactPresenter presenter;
    private ContactAdapter mAdapter;

    ArrayList<Contact> allContacts = new ArrayList<>();
    String operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ButterKnife.bind(this);

        textSelectedContactsAllContacts = (TextView) findViewById(R.id.selectedContactsAllContacts);
        generalCheckbox = (AppCompatCheckBox) findViewById(R.id.checkedAllContact);

        // We created the presenter
        presenter = new ContactPresenter(this);

        // Set the view
        setUpView();

        // Set the general checkbox
        generalCheckbox.setChecked(presenter.chekedOrUnckeked(getSelectedContactsId(), allContacts));

        // Set the textView:  Selected contacts / all contacts
        textSelectedContactsAllContacts.setText(loadSelectedContactsAllContacts());
    }

    /**
     * Set up the View.
     */
    private void setUpView() {
        setUpToolbar();
        loadAllMediaFiles(presenter.getOperationType(), presenter.getContactsList());
    }

    /**
     * Set up the Toolbar.
     */
    private void setUpToolbar() {
        setSupportActionBar(mToolbarContactActivity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void loadAllMediaFiles(String operationType, final ArrayList<Contact> contactsList) {

        allContacts = contactsList;
        operation = operationType;

        mAdapter = new ContactAdapter(this, contactsList);

        mContactsRecycle.setAdapter(mAdapter);
        mContactsRecycle.setHasFixedSize(true);
        mContactsRecycle.setLayoutManager(new LinearLayoutManager(this));

        updateAllCheckbox(operationType);
    }

    /**
     * Update all checkbox
     *
     * @param operationType
     */
    public void updateAllCheckbox(String operationType) {

        ArrayList<String> listOfid;

        if (operationType.equals("export")) {
            listOfid = BackupSingleton.getInstance().getContactListOfId();
        } else {
            listOfid = RestoreSingleton.getInstance().getContactListOfId();
        }

        // Check the condition of all checkbox
        if (!generalCheckbox.isChecked() && listOfid.size() < 1) {
            for (Contact contact : allContacts) {
                contact.setSelected(generalCheckbox.isChecked());
            }
        } else {

            Map<String, Contact> contactMap = new LinkedHashMap<>();

            for (Contact contact : allContacts) {

                for (String id : listOfid) {

                    if (contact.getContactId().equals(id)) {
                        contact.setSelected(true);
                        contactMap.put(contact.getContactId(), contact);
                    }
                }

                if (!contactMap.containsKey(contact.getContactId())) {
                    contact.setSelected(false);
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Load the Selected Contacts All Contacts in string
     *
     * @return
     */
    public String loadSelectedContactsAllContacts() {

        ArrayList<Contact> selectedContacts = new ArrayList<>();

        for (Contact contact : allContacts) {
            if (contact.isSelected()) {
                selectedContacts.add(contact);
            }
        }
        return selectedContacts.size() + "\t / \t" + allContacts.size();
    }

    /**
     * Checked or uncheked all contacts
     *
     * @param checked
     */
    public void chekedUnchekedAllContacts(boolean checked) {

        for (Contact contact : allContacts) {
            contact.setSelected(checked);
        }
        mAdapter.notifyDataSetChanged();      //Notify changes to adapter
    }

    /**
     * Get id's from selected contacts
     *
     * @return mSelectedContacts
     */
    public ArrayList<String> getSelectedContactsId() {

        ArrayList<String> mSelectedContacts = new ArrayList<>();

        for (Contact contact : allContacts) {
            if (contact.isSelected()) {
                mSelectedContacts.add(contact.getContactId());
            }
        }

        return mSelectedContacts;
    }

    /**
     * Update Object (Backup Or Restore)
     */
    public void updateObject() {

        if (operation.equals("export")) {
            // Update the contacts list and id from selected contacts in Backup object
            BackupSingleton.getInstance().setContactListOfId(getSelectedContactsId());
            BackupSingleton.getInstance().setContactsList(mAdapter.getContactsList());
        } else {
            //Update the contacts list and id from selected contacts in Restore object
            RestoreSingleton.getInstance().setContactListOfId(getSelectedContactsId());
            RestoreSingleton.getInstance().setContactsList(mAdapter.getContactsList());
        }
    }

    @OnClick({R.id.checkedAllContact, R.id.contact_action_buttom})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkedAllContact:

                chekedUnchekedAllContacts(generalCheckbox.isChecked());

                // Update the textView if general checkbox is checked or unchecked
                textSelectedContactsAllContacts.setText(loadSelectedContactsAllContacts());

                break;

            case R.id.contact_action_buttom:

                updateObject();

                Intent intent = new Intent(this, HomeActivity.class);
                intent.putExtra("operation", operation);
                setResult(RESULT_OK, intent);
                finish();

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    //------------ Interface methods

    /**
     * Set textView
     * @param selectedContactsAllContacts
     */
    @Override
    public void setTheTextViewOfSelectedContactsAllContacts(String selectedContactsAllContacts) {
        textSelectedContactsAllContacts.setText(selectedContactsAllContacts);
    }

    /**
     * Set the general checkbox
     * @param checked
     */
    @Override
    public void setCheckedTheGeneralCheckBox(boolean checked) {
        generalCheckbox.setChecked(checked);
    }
}