package org.escoladeltreball.freebackup.ui.adapter;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.escoladeltreball.freebackup.R;
import org.escoladeltreball.freebackup.intefaces.ContactDataTransfer;
import org.escoladeltreball.freebackup.model.Contact;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by boty on 29/04/16.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    ArrayList<Contact> contactslist;
    private ContactDataTransfer mContactDataTransfer;

    public ContactAdapter(ContactDataTransfer contactDataTransfer, ArrayList<Contact> contactslist) {
        this.contactslist = contactslist;
        this.mContactDataTransfer = contactDataTransfer;

    }

    @Override
    public int getItemCount() {
        return contactslist.size();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_item, viewGroup, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder viewHolder, int i) {

        final int position = i;

        viewHolder.contactName.setText(contactslist.get(i).getContactName());

        boolean hasMobile = contactslist.get(i).getContactMobile().equals(""),
                hasMail = contactslist.get(i).getContactMail().equals(""),
                hasOthers = contactslist.get(i).getContactOthers().equals("");

        // In case you have synchronized your contacts with google to not display the profile of the contact in the list
        Pattern pat = Pattern.compile("^http.*");
        Matcher mailMatcher = pat.matcher(contactslist.get(i).getContactMail());
        Matcher othersMatcher = pat.matcher(contactslist.get(i).getContactOthers());

        if (!hasMobile && (!hasMail || !hasOthers)) {
            viewHolder.contactNumber.setText(contactslist.get(i).getContactMobile() + "\t/ more...");
        } else if (hasMobile && !hasMail && !mailMatcher.matches()) {
            viewHolder.contactNumber.setText(contactslist.get(i).getContactMail());
        } else if (hasMobile && (hasMail || mailMatcher.matches()) && !hasOthers && !othersMatcher.matches()) {
            viewHolder.contactNumber.setText(contactslist.get(i).getContactOthers());
        } else {
            viewHolder.contactNumber.setText(contactslist.get(i).getContactMobile());
        }

        viewHolder.contactSelected.setChecked(contactslist.get(i).isSelected());

        viewHolder.contactSelected.setTag(contactslist.get(i));

        viewHolder.contactSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppCompatCheckBox cb = (AppCompatCheckBox) v;

                Contact contact = (Contact) cb.getTag();
                contact.setSelected(cb.isChecked());

                contactslist.get(position).setSelected(cb.isChecked());

                //We update the TextView and general checkbox
                mContactDataTransfer.setTheTextViewOfSelectedContactsAllContacts(updateSelectedContacts().size() + "\t / \t" + contactslist.size());
                mContactDataTransfer.setCheckedTheGeneralCheckBox(updateGeneralCheckbox());
            }
        });
    }

    public ArrayList<Contact> updateSelectedContacts() {

        ArrayList<Contact> contactsSelected = new ArrayList<>();

        for (Contact contact : contactslist) {

            if (contact.isSelected()) {
                contactsSelected.add(contact);
            }
        }
        return contactsSelected;
    }

    public boolean updateGeneralCheckbox() {

        boolean status;

        if (updateSelectedContacts().size() < contactslist.size()) {
            status = false;
        } else {
            status = true;
        }
        return status;
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.contactName)
        TextView contactName;

        @Bind(R.id.contactInfo)
        TextView contactNumber;

        @Bind(R.id.contactCkeckBox)
        AppCompatCheckBox contactSelected;

        public ContactViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public ArrayList<Contact> getContactsList() {
        return contactslist;
    }
}