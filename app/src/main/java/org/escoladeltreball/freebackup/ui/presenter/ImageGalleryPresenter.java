package org.escoladeltreball.freebackup.ui.presenter;

import org.escoladeltreball.freebackup.model.BackupSingleton;
import org.escoladeltreball.freebackup.model.Image;
import org.escoladeltreball.freebackup.ui.activity.ImageGalleryActivity;

import java.util.ArrayList;

/**
 * Gallery Presenter.
 *
 * Created by Arthur on 9/5/16.
 */
public class ImageGalleryPresenter {

    private ImageGalleryActivity view;

    public ImageGalleryPresenter(ImageGalleryActivity view) {
        this.view = view;
    }

    /**
     * Get images files from Backup Singleton Object.
     */
    public void getAllMediaFiles() {
        //TODO: mostrar ProgresDialog
        ArrayList<Image> images = BackupSingleton.getInstance().getImagesList();
        view.loadMediaFilesOnAdapter(images);
    }


    /**
     * Set all images of list as selected or unselected.
     *
     * @param state the state for set all images as selected or not.
     */
    public void setAllListAsSelected(boolean state) {

        for (Image image : BackupSingleton.getInstance().getImagesList()) {
            image.setSelected(state);
        }
    }

    public void upDatePathOfImagesSelected(){

        ArrayList<String> pathsOfImagesSelected = new ArrayList<>();

        for (Image image : BackupSingleton.getInstance().getImagesList()) {

            if(image.isSelected()){
                pathsOfImagesSelected.add(image.getPathFile());
            }
        }

        BackupSingleton.getInstance().setPathOfImagesSelected(pathsOfImagesSelected);

        BackupSingleton.getInstance().setImagesTotalSelected(BackupSingleton.getInstance()
                .getPathOfImagesSelected().size());
        
    }

}
